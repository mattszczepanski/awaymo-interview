# Awaymo Interview
######  Wroclaw, 28 november 2018


#### RUN:
```buildoutcfg
docker-compose up --build
```

## Sign up service:

```buildoutcfg
/api/users
/api/users/<pk>
/api/users/<pk>/update_user
/api/users/<pk>/validate
```

#### api/users

- GET all users

#### /api/users/<user_id>

- GET user

#### /api/users/<pk>/update_user

- Update information ['first_name', 'last_name', 'phone_number', 'birthday']

#### /api/users/<user_id>/validate

- Send POST request to 3dPartyAPI and verifies user

## Step builder
Step builder would require from developer to create class that inherit from Step.

This idea comes from Template Design Pattern. 

Each step can have individual model / use existing one.

Each step have individual view and at least one url.

Step manager then will return all urls for each step in children steps.

###### This is unfinished due to lack knowledge about Django.



## Ideas:
- Oauth2
- Microservice for 3rd party credit check API (error handling)
- Development / Production / Staging / Test enviroment configurations
- Proper CI/CD deployment flow 
- Sweet react flow
- Atuhentication microservice for whole project and other company projects (manage access rights etc.)


## TODO:
##### TRELLO BOARD :  https://trello.com/b/IujqoB3q/rest-api-sign-up-system

- Django Project Structure, basic gitlab CI/CD: 1h
- Database Structure / Django Models : 1h
- View CreateUserAccount : 1h
- View UserAdditionalInfo : 3h
- View UserCreditAcceptance : 3h
- AUTH : 1h
- Simple frontend : 0.5h
- TESTS : 3h
- Docker : 0.5h


###### Awaymo Sign Up  by Mateusz Szczepański
