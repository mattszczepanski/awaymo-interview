from steps.step import Step
from django.db import models


class SampleStep(Step):
    @classmethod
    def models(cls):
        class Model(models.Model):
            id = models.AutoField(primary_key=True)
            sample_value = models.IntegerField()

            def create(cls, data):
                for row in data:
                    m = Model(sample_value=row['sample_value'])
                    m.save()

        cls.model = Model

    @classmethod
    def views(cls):
        '''TO BE DEFINED'''
        pass
