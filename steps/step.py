import abc


class Step(metaclass=abc.ABCMeta):
    URL_ROUTE = None

    @abc.abstractmethod
    def post(self):
        pass

    @abc.abstractmethod
    def get(self):
        pass

    @abc.abstractmethod
    def models(self):
        '''
        :return:
        '''
        pass

    @abc.abstractmethod
    def views(self):
        pass

    @abc.abstractmethod
    def urls(self):
        '''
        This method should return urls for steps
        :return: path(self.route, self.view.as_view())
        '''
        pass
