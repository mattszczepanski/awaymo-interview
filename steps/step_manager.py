import abc
from collections import defaultdict


class StepsManager(object):
    class __metaclass__(type):
        __inheritors__ = defaultdict(list)

        def __new__(meta, name, bases, dct):
            klass = type.__new__(meta, name, bases, dct)
            for base in klass.mro()[1:-1]:
                meta.__inheritors__[base].append(klass)
            return klass

    @abc.abstractmethod
    def urls(self):
        '''
        Builds routes for all children steps
        '''
        return [step.urls for step in self.__inheritors__]
