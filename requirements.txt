django==2.1.3
djangorestframework==3.9.0
requests==2.20.1
psycopg2==2.7.6.1
django-phonenumber-field==2.1.0
phonenumbers==8.10.1