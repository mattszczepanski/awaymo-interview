from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

import requests

from server.serializers import UserSerializer, UserWriteSerializer, UserInfoSerializer
from .models import User

THIRD_PARTY_CREDIT_CHECK = 'https://functionapp20180527095701.azurewebsites.net/api/CreditCheckCall'
CONTENT = 'content'
FIRST_NAME = "first_name"
LAST_NAME = "last_name"
BIRTHDAY = "birthday"
PHONE_NUMBER = "phone_number"
EMAIL = "email"
ACCEPTED = 'accepted'
VERIFIED = 'verified'
UPDATED = 'updated'


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get_serializer_class(self):
        if self.action in ['list', 'retrieve']:
            return UserSerializer
        return UserWriteSerializer

    def perform_create(self, serializer):
        user = serializer.save()
        user.set_password(self.request.data.get('password'))
        user.save()

    def perform_update(self, serializer):
        user = serializer.save()
        if 'password' in self.request.data:
            user.set_password(self.request.data.get('password'))
            user.save()

    @action(detail=True, methods=['GET'])
    def validate(self, request, pk=None, *args, **kwargs):
        user = self.get_object()
        is_verified = self.verify(user.first_name, user.last_name)
        user.verified = is_verified
        user.save()
        return Response({CONTENT: VERIFIED}, status=status.HTTP_200_OK)

    @staticmethod
    def verify(first_name, last_name):
        payload = {FIRST_NAME: first_name,
                   LAST_NAME: last_name}
        response = requests.post(THIRD_PARTY_CREDIT_CHECK, json=payload)
        if status.is_success(response.status_code):
            return response.json()[ACCEPTED]


class UserDetailViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserInfoSerializer
    permission_classes = (IsAuthenticated,)

    @action(detail=True, methods=['POST'])
    def update_user(self, request, pk=None, *args, **kwargs):
        user = self.get_object()
        serializer = UserInfoSerializer(data=request.data)
        if serializer.is_valid():
            user.first_name = self.request.data.get(FIRST_NAME)
            user.last_name = self.request.data.get(LAST_NAME)
            user.phone_number = self.request.data.get(PHONE_NUMBER)
            user.birthday = self.request.data.get(BIRTHDAY)
            user.save()
            return Response({CONTENT: UPDATED}, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
