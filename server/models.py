from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils import timezone
from phonenumber_field.modelfields import PhoneNumberField


class UserManager(BaseUserManager):
    def _create_user(self, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        user = self.model(email=self.normalize_email(email),
                          is_active=True,
                          is_staff=is_staff,
                          is_superuser=is_superuser,
                          last_login=timezone.now(),
                          registered_at=timezone.now(),
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, **extra_fields):
        return self._create_user(email, password, **extra_fields)


class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='Email', unique=True, max_length=255)
    password = models.TextField(max_length=64)
    first_name = models.CharField(verbose_name='First name', max_length=30, default='first')
    last_name = models.CharField(verbose_name='Last name', max_length=30, default='last')
    birthday = models.DateField(blank=False, default='1980-01-01')
    phone_number = PhoneNumberField(blank=False, default='555444333')
    verified = models.BooleanField(default=False)

    # Fields settings
    EMAIL_FIELD = 'email'
    USERNAME_FIELD = 'email'

    objects = UserManager()

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    @property
    def full_name(self):
        return f'{self.first_name} {self.last_name}'

    full_name.fget.short_description = 'Full name'

    def get_full_name(self):
        return self.full_name

    def __str__(self):
        return self.full_name
