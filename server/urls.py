from django.urls import path, include
from rest_framework import routers
from .views import UserViewSet, UserDetailViewSet

api = routers.DefaultRouter()
api.trailing_slash = '/?'

# Users API
api.register(r'users', UserViewSet)
api.register(r'users', UserDetailViewSet)

urlpatterns = [

    path('api/', include(api.urls))

]
